#include <Arduino.h>
#include <WiFi.h>
#include <Preferences.h>
#include <Adafruit_Sensor.h>
#include "lmic.h"
#include <hal/hal.h>
#include <SPI.h>
#include <DHT.h>
#include <SSD1306.h>
#include <mySD.h>

// Lora frequency
#define LORA_PERIOD 868

#define PIN_LEDPIN 25
#define PIN_VBAT 35

// DHT11 - Temp & Humidity sensor
//#define DHTPIN 15
//#define DHTTYPE DHT11
//DHT dht(DHTPIN, DHTTYPE);

// OLED Display
#define OLED_I2C_ADDR 0x3C
#define OLED_RESET 16
#define OLED_SDA_V2 21
#define OLED_SCL_V2 22
SSD1306 display(OLED_I2C_ADDR, OLED_SDA_V2, OLED_SCL_V2);

// NVRam settings
// This is flash memory on the ESP32
// and is persistent on power outage
//Preferences preferences;

// Copy the value from Device EUI from the TTN console in LSB mode.
// We are using WiFi macAddress - see setup() function.
u1_t PROGMEM DEVEUI[8];
void os_getDevEui(u1_t *buf) { memcpy_P(buf, DEVEUI, 8); }

// Copy the value from Application EUI from the TTN console in LSB mode
static const u1_t PROGMEM APPEUI[8] = { 0x9A, 0xDD, 0x01, 0xD0, 0x7E, 0xD5, 0xB3, 0x70 };
void os_getArtEui(u1_t *buf) { memcpy_P(buf, APPEUI, 8); }

// This key should be in big endian format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from ttnctl can be copied as-is. Anyway its in MSB mode.
static const u1_t PROGMEM APPKEY[16] = { 0xDC, 0x59, 0xC9, 0x58, 0x14, 0x93, 0x30, 0x18, 0xD7, 0xF1, 0xD6, 0x4A, 0xE9, 0x83, 0x9B, 0xCD };
void os_getDevKey(u1_t *buf) { memcpy_P(buf, APPKEY, 16); }

static osjob_t sendjob;

// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
//const unsigned TX_INTERVAL = 120;
const unsigned TX_INTERVAL = 15;

// Pins for the TTGO Lora32 with 3D metal antenna
const lmic_pinmap lmic_pins = {
    .nss = 18,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 14,
    .dio = {26, 33, 32} 
};

void do_send(osjob_t *j)
{
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND)
    {
        Serial.println(F("OP_TXRXPEND, not sending"));
    }
    else
    {
        float vdd_value1 = analogRead(PIN_VBAT);
        delay(100);
        float vdd_value2 = analogRead(PIN_VBAT);
        delay(100);
        float vdd_value3 = analogRead(PIN_VBAT);

        float vdd_value = (vdd_value1 + vdd_value2 + vdd_value3) / 3;
        vdd_value = vdd_value * (4.2f / 4095.0f);

        float VBAT = (127.0f / 100.0f) * 4.20f * float(analogRead(PIN_VBAT)) / 4095.0f; // LiPo battery
        Serial.printf("VBAT Readings: %f\n", VBAT);

        // Prepare the VDD elsys format payload
        // lowByte() & highByte() functions could be used.
        unsigned int elsysVdd = std::floor(VBAT * 1000);

        unsigned char elsysVddLow = elsysVdd & 0xFF;
        unsigned char elsysVddHigh = (elsysVdd >> 8) & 0xFF;

        //uint8_t message[8] = {0x01, elsysTempHigh, elsysTempLow, 0x02, elsysHumidityLow, 0x07, elsysVddHigh, elsysVddLow};
        uint8_t message[] = {0x07, elsysVddHigh, elsysVddLow};

        display.drawString(0, 24, String("VDD Readings: ") + String(vdd_value));
        display.display();

        // prepare upstream data transmission at the next possible time.
        // transmit on port 1 (the first parameter); you can use any value from 1 to 223 (others are reserved).
        // don't request an ack (the last parameter, if not zero, requests an ack from the network).
        // Remember, acks consume a lot of network resources; don't ask for an ack unless you really need it.
        LMIC_setTxData2(5, message, sizeof(message), 0);
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

void onEvent(ev_t ev)
{
    Serial.print(os_getTime());
    Serial.print(": ");
    switch (ev)
    {
    case EV_SCAN_TIMEOUT:
        Serial.println(F("EV_SCAN_TIMEOUT"));
        break;
    case EV_BEACON_FOUND:
        Serial.println(F("EV_BEACON_FOUND"));
        break;
    case EV_BEACON_MISSED:
        Serial.println(F("EV_BEACON_MISSED"));
        break;
    case EV_BEACON_TRACKED:
        Serial.println(F("EV_BEACON_TRACKED"));
        break;
    case EV_JOINING:
        // Joining LoraWAN network
        Serial.println(F("EV_JOINING: -> Joining..."));
        display.drawString(0, 12, F("OTAA joining..."));
        display.display();

        break;
    case EV_JOINED:
        // Event - joined LoraWAN network
        Serial.println(F("EV_JOINED"));
        //display.clear();
        display.drawString(80, 12, "Joined!");
        display.display();
        Serial.println(F("EV_JOINED"));

        // Disable link check validation (automatically enabled
        // during join, but because slow data rates change max TX
        // size, we don't use it in this example.
        LMIC_setLinkCheckMode(0);
        break;
    case EV_JOIN_FAILED:
        Serial.println(F("EV_JOIN_FAILED"));
        break;
    case EV_REJOIN_FAILED:
        Serial.println(F("EV_REJOIN_FAILED"));
        break;
    case EV_TXCOMPLETE:
        Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));

        if (LMIC.txrxFlags & TXRX_ACK)
            Serial.println(F("Received ack"));
        if (LMIC.dataLen) {
            // data received in rx slot after tx
            Serial.print(F("Received "));
            Serial.print(LMIC.dataLen);
            Serial.println(F(" bytes of payload:"));
            for (int i = 0; i < LMIC.dataLen; i++) {
                Serial.printf("0x%02X ", LMIC.frame[LMIC.dataBeg + i]);
            }
            Serial.println();
        }
        // Schedule next transmission
        os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
        break;
    case EV_LOST_TSYNC:
        Serial.println(F("EV_LOST_TSYNC"));
        break;
    case EV_RESET:
        Serial.println(F("EV_RESET"));
        break;
    case EV_RXCOMPLETE:
        // data received in ping slot
        Serial.println(F("EV_RXCOMPLETE"));
        break;
    case EV_LINK_DEAD:
        Serial.println(F("EV_LINK_DEAD"));
        break;
    case EV_LINK_ALIVE:
        Serial.println(F("EV_LINK_ALIVE"));
        break;
    default:
        Serial.print(F("Unknown event: "));
        Serial.println((unsigned)ev);
        break;
    }
}

void setup()
{
    Serial.begin(115200);

    while (!Serial)
        ;

    // Give some time to connect to serial
    delay(5000);

    char macAddress[18];
    char * pch;
    int idx = 7;
    
    WiFi.mode(WIFI_MODE_STA);
    WiFi.macAddress().toCharArray(macAddress, 18, 0);
    WiFi.mode(WIFI_OFF);
    btStop();

    Serial.println(macAddress);

    pch = strtok (macAddress,":");

    DEVEUI[idx--] = 0x00;
    DEVEUI[idx--] = 0x00;
    while (pch != NULL)
    {
        DEVEUI[idx--] = strtol(pch, NULL, 16);
        pch = strtok (NULL, ":");
    }   

    Serial.printf("DEVEUI = { 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X //lsb\n", DEVEUI[7], DEVEUI[6], DEVEUI[5], DEVEUI[4], DEVEUI[3], DEVEUI[2], DEVEUI[1], DEVEUI[0]);

    display.init();
    //display.flipScreenVertically ();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setBrightness(50);

    // LMIC init - initialize library for LoraWAN
    os_init();

    // Reset the MAC state. Session and pending data transfers will be discarded.
    // This resets all sessions to defaults
    LMIC_reset();

    Serial.println("LoRa Initializing OK!");
    display.drawString(0, 0, "LoRa Initializing OK!");
    display.display();

    // Sets the max clock error to compensate for - see definition for more info
    LMIC_setClockError(MAX_CLOCK_ERROR * 1 / 100);

    // Set up the channels used by the Things Network, which corresponds
    // to the defaults of most gateways. Without this, only three base
    // channels from the LoRaWAN specification are used, which certainly
    // works, so it is good for debugging, but can overload those
    // frequencies, so be sure to configure the full frequency range of
    // your network here (unless your network autoconfigures them).
    // Setting up channels should happen after LMIC_setSession, as that
    // configures the minimal channel set.

    LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI); // g-band
    LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK, DR_FSK), BAND_MILLI);   // g2-band

    // TTN defines an additional channel at 869.525Mhz using SF9 for class B
    // devices' ping slots. LMIC does not have an easy way to define set this
    // frequency and support for class B is spotty and untested, so this
    // frequency is not configured here.

    // Disable link check validation
    LMIC_setLinkCheckMode(0);

    // TTN uses SF9 for its RX2 window.
    LMIC.dn2Dr = DR_SF9;

    // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
    //LMIC_setDrTxpow(DR_SF11,14);
    LMIC_setDrTxpow(DR_SF9, 14);

    // Start job
    do_send(&sendjob); // Will fire up also the join
    LMIC_startJoining();
}

void loop()
{
    os_runloop_once();
}