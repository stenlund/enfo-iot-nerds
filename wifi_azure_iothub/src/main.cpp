#include <WiFi.h>
#include "AzureIotHub.h"
#include "Esp32MQTTClient.h"
#include "FS.h"
#include "SPIFFS.h"

#define INTERVAL 10000
#define DEVICE_ID "myDevice"
#define MESSAGE_MAX_LEN 256
#define LED_BUILTIN 25

// Please input the SSID and password of WiFi
char ssid[] = "Demo";
const char* password = "Demo-2019";


/*String containing Hostname, Device Id & Device Key in the format:                         */
/*  "HostName=<host_name>;DeviceId=<device_id>;SharedAccessKey=<device_key>"                */
/*  "HostName=<host_name>;DeviceId=<device_id>;SharedAccessSignature=<device_sas_token>"    */
static const char* connectionString = "HostName=XXXX.azure-devices.net;DeviceId=myDevice;SharedAccessKey=XXXX";

const char *messageData = "{\"dev_id\":\"%s\", \"msg_id\":%d, \"payload_raw\": { \"temperature\":%f, \"humidity\":%f, \"motion\": %d} }";

int messageCount = 1;
static bool hasWifi = false;
static bool messageSending = true;
static uint64_t send_interval_ms;

///////////////////////////////////////////////////
// Utilities
///////////////////////////////////////////////////
static void InitWifi()
{
  Serial.println("Connecting...");
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  hasWifi = true;
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

static void SendConfirmationCallback(IOTHUB_CLIENT_CONFIRMATION_RESULT result)
{
  if (result == IOTHUB_CLIENT_CONFIRMATION_OK)
  {
    Serial.println("Send Confirmation Callback finished.");

    File seqFile = SPIFFS.open("/msg.seq", "w");
    if (!seqFile) {
        Serial.println("ERROR: Could not open seq no file for writing!");
    } else {
        Serial.println("Writing seq no to file.");
        seqFile.println(messageCount);
    }
    seqFile.close();
    digitalWrite(LED_BUILTIN, HIGH);
  }
}

static void MessageCallback(const char* payLoad, int size)
{
  Serial.println("Message callback:");
  Serial.println(payLoad);
}

static void DeviceTwinCallback(DEVICE_TWIN_UPDATE_STATE updateState, const unsigned char *payLoad, int size)
{
  char *temp = (char *)malloc(size + 1);
  if (temp == NULL)
  {
    return;
  }
  memcpy(temp, payLoad, size);
  temp[size] = '\0';
  // Display Twin message.
  Serial.println(temp);
  free(temp);
}

static int  DeviceMethodCallback(const char *methodName, const unsigned char *payload, int size, unsigned char **response, int *response_size)
{
  LogInfo("Try to invoke method %s", methodName);
  const char *responseMessage = "\"Successfully invoke device method\"";
  int result = 200;

  if (strcmp(methodName, "start") == 0)
  {
    LogInfo("Start sending temperature and humidity data");
    messageSending = true;
  }
  else if (strcmp(methodName, "stop") == 0)
  {
    LogInfo("Stop sending temperature and humidity data");
    messageSending = false;
  }
  else
  {
    LogInfo("No method %s found", methodName);
    responseMessage = "\"No method found\"";
    result = 404;
  }

  *response_size = strlen(responseMessage) + 1;
  *response = (unsigned char *)strdup(responseMessage);

  return result;
}

///////////////////////////////////////////////////
// Arduino framework - setup()
//  - Preparation for main loop
///////////////////////////////////////////////////
void setup()
{
  Serial.begin(115200);
  Serial.println("ESP32 Device");
  Serial.println("Initializing...");
  
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  // Prepare file system
  if(!SPIFFS.begin(true)){
      Serial.println("An Error has occurred while mounting SPIFFS");
      return;
  }
  File seqFile = SPIFFS.open("/msg.seq", "r");
  if ( seqFile && seqFile.size() ) {
    Serial.println("Existing msg sequence file for reading");
    String seqNo = seqFile.readString();
    seqFile.close();
    int iSeqNo = seqNo.toInt();
    Serial.print("SeqNo: ");
    Serial.println(iSeqNo);
    messageCount = iSeqNo;
  }

  // Initialize the WiFi module
  Serial.println(" > WiFi");
  hasWifi = false;
  InitWifi();
  if (!hasWifi)
  {
    return;
  }
  randomSeed(analogRead(0));

  Serial.println(" > IoT Hub");
  Esp32MQTTClient_SetOption(OPTION_MINI_SOLUTION_NAME, "IOT_Demo");
  Esp32MQTTClient_Init((const uint8_t*)connectionString, true);

  Esp32MQTTClient_SetSendConfirmationCallback(SendConfirmationCallback);
  Esp32MQTTClient_SetMessageCallback(MessageCallback);
  Esp32MQTTClient_SetDeviceTwinCallback(DeviceTwinCallback);
  Esp32MQTTClient_SetDeviceMethodCallback(DeviceMethodCallback);

  send_interval_ms = millis();
}


///////////////////////////////////////////////////
// Arduino framework - loop()
//  - Main execution
///////////////////////////////////////////////////
void loop()
{
  if (hasWifi)
  {
    if (messageSending && 
        (int)(millis() - send_interval_ms) >= INTERVAL)
    {
      digitalWrite(LED_BUILTIN, LOW);

      // Simulate measures
      float temperature = (float) 20.0 + (float) random(0,100) / 10;
      float humidity = (float)random(0, 1000)/10;
      int motion = messageCount % 2 == 0 ? 1 : 0;

      // Format message payload
      char messagePayload[MESSAGE_MAX_LEN];
      snprintf(messagePayload,MESSAGE_MAX_LEN, messageData, DEVICE_ID, messageCount++, temperature, humidity, motion);
      Serial.println(messagePayload);

      // Create MQTT message, add property
      EVENT_INSTANCE* message = Esp32MQTTClient_Event_Generate(messagePayload, MESSAGE);
      if(temperature > 25)
        Esp32MQTTClient_Event_AddProp(message, "temperatureAlert", "true");
 
      Esp32MQTTClient_SendEventInstance(message);
      
      send_interval_ms = millis();
    }
    else
    {
      Esp32MQTTClient_Check();
    }
  }
  delay(10);
}
