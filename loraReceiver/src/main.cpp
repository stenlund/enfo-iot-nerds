#include <SPI.h>
#include <LoRa.h>
#include <SSD1306.h>

//define the pins used by the transceiver module
#define ss 18
#define rst 14
#define dio0 26

// OLED 
#define OLED_I2C_ADDR 0x3C
#define OLED_RESET 16
#define OLED_SDA 21
#define OLED_SCL 22
SSD1306 display (OLED_I2C_ADDR, OLED_SDA, OLED_SCL);

int counter = 0;

void setup(){
  Serial.begin(115200);
  while (!Serial);

  display.init();
  //display.flipScreenVertically ();
  display.setFont (ArialMT_Plain_10);
  display.setTextAlignment (TEXT_ALIGN_LEFT);
  display.setBrightness(50);


  Serial.println("LoRa Receiver");
  display.drawString (0, 0, "LoRa Receiver");
  display.display();
  
  //setup LoRa transceiver module
  LoRa.setPins(ss, rst, dio0);
  
  //replace the LoRa.begin(---E-) argument with your location's frequency 
  //433E6 for Asia
  //868E6 for Europe
  //915E6 for North America
  while (!LoRa.begin(868E6)) {
    Serial.println(".");
    delay(500);
  }

  // Change sync word (0xF3) to match the receiver
  // The sync word assures you don't get LoRa messages from other LoRa transceivers
  // ranges from 0-0xFF
  LoRa.setSyncWord(0xF3);
  Serial.println("LoRa Initializing OK!");
  display.drawString (0, 12, "LoRa Initializing OK!");
  display.display();
}

void loop() {
  // try to parse packet
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    // received a packet
    Serial.print("Received packet '");
    display.setColor(BLACK);
    display.fillRect(0, 24, 150, 48);
          
    // read packet
    while (LoRa.available()) {
      String LoRaData = LoRa.readString();
      Serial.print(LoRaData); 

      display.setColor(WHITE);
      display.drawString (0, 24, String("Received: ") + String(LoRaData));
      display.display();
    }

    // print RSSI of packet
    Serial.print("' with RSSI ");
    Serial.println(LoRa.packetRssi());
    display.drawString (0, 36, String("RSSI: ") + String(LoRa.packetRssi())); 
    display.display();
  }
}