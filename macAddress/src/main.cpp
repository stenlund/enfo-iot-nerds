#include <Arduino.h>
#include "WiFi.h"
#include <SSD1306.h>

// OLED 
#define OLED_I2C_ADDR 0x3C
#define OLED_RESET 16
#define OLED_SDA 21
#define OLED_SCL 22
SSD1306 display (OLED_I2C_ADDR, OLED_SDA, OLED_SCL);

void setup(){
  Serial.begin(9600);
 
  WiFi.mode(WIFI_MODE_STA);

  display.init();
  //display.flipScreenVertically ();
  display.setFont (ArialMT_Plain_10);
  display.setTextAlignment (TEXT_ALIGN_LEFT);
  display.setBrightness(50);
  display.drawString (0, 0, WiFi.macAddress());
  display.display();
}
 
void loop(){
    Serial.println(WiFi.macAddress());
    delay(1000);
}